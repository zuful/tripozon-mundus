package main

type UserStructure struct {
	Id               string            `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Firstname        string            `protobuf:"bytes,3,opt,name=firstname,proto3" json:"firstname,omitempty"`
	Lastname         string            `protobuf:"bytes,4,opt,name=lastname,proto3" json:"lastname,omitempty"`
	Email            string            `protobuf:"bytes,5,opt,name=email,proto3" json:"email,omitempty"`
	Password         string            `protobuf:"bytes,6,opt,name=password,proto3" json:"password,omitempty"`
	Mobile           string            `protobuf:"bytes,7,opt,name=mobile,proto3" json:"mobile,omitempty"`
	Address          *AddressStructure `protobuf:"bytes,8,opt,name=address,proto3" json:"address,omitempty"`
	AvatarImgUrl     string            `protobuf:"bytes,9,opt,name=avatarImgUrl,proto3" json:"avatarImgUrl,omitempty"`
	IsDisabled       bool              `protobuf:"varint,10,opt,name=isDisabled,proto3" json:"isDisabled,omitempty"`
	EmailVerified    bool              `protobuf:"varint,11,opt,name=emailVerified,proto3" json:"emailVerified,omitempty"`
	IsOnline         bool              `protobuf:"varint,12,opt,name=isOnline,proto3" json:"isOnline,omitempty"`
	FcmToken         string            `protobuf:"varint,12,opt,name=fcmToken,proto3" json:"fcmToken,omitempty"`
	CreationDate     string            `protobuf:"bytes,13,opt,name=creationDate,proto3" json:"creationDate,omitempty"`
	ModificationDate string            `protobuf:"bytes,14,opt,name=modificationDate,proto3" json:"modificationDate,omitempty"`
}
