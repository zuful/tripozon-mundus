package main

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"time"
)

const (
	port = ":50051"
)

func main() {

	r := gin.Default()
	// CORS for https://foo.com and https://github.com origins, allowing:
	// - PUT and PATCH methods
	// - Origin header
	// - Credentials share
	// - Preflight requests cached for 12 hours

	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{
			"http://localhost:8100",
			"http://localhost:4200",
			"https://messenger-1670c.firebaseapp.com",
			"https://tripozon-pro.firebaseapp.com",
		},
		AllowMethods:     []string{"PUT", "PATCH"},
		AllowHeaders:     []string{"Origin", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length", "Content-Type"},
		AllowCredentials: true,
		/*AllowOriginFunc: func(origin string) bool {
			return origin == "https://github.com"
		},*/
		MaxAge: 12 * time.Hour,
	}))

	r.Use(cors.Default()) //allows all origins (must but changed when put in production)

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	r.GET("/user/:userId", func(c *gin.Context) {

		var userId string = c.Param("userId")
		var user *UserStructure = GetUser(userId)

		c.JSON(200, user)
	})

	r.POST("/user", func(c *gin.Context) {

		var user *UserStructure

		//prevents from unmarshalling json and binds the route param directly to the type
		err := c.Bind(&user)
		if err != nil {
			log.Println(err)
		}

		setUserId(user) //generates a uuid for the user
		err = CreateUser(user)

		c.JSON(200, err)
	})

	r.POST("/sign-up", func(c *gin.Context) {

		var msg string
		var err error
		var code int
		var user *UserStructure

		//prevents from unmarshalling json and binds the route param directly to the type
		err = c.Bind(&user)

		if err != nil {
			log.Println(err)
		} else {

			setUserId(user) //generates a uuid for the user

			err = CreateUser(user) //create a user in the user collection
			if err == nil {
				err = SignUp(user) //create a user in the firebase authentication service
			}

		}

		if err != nil {

			msg = err.Error()
			code = http.StatusInternalServerError

		} else {

			msg = "Success"
			code = http.StatusOK

		}

		c.JSON(code, msg)
	})

	r.POST("/send-notification", func(c *gin.Context) {

		var notification NotificationStructure

		//prevents from unmarshalling json and binds the route param directly to the type
		err := c.Bind(&notification)
		if err != nil {
			log.Println(err)
		}

		res := sendFcmNotification(notification)
		c.JSON(200, res)
	})

	r.PATCH("/user/:userId", func(c *gin.Context) {

		var userId string = c.Param("userId")
		var users UserStructure

		//prevents from unmarshalling json and binds the route param directly to the type
		err := c.Bind(&users)
		if err != nil {
			log.Println(err)
		}

		response := UpdateUser(userId, users)

		c.JSON(200, response)
	})

	r.PATCH("/user-fcm-token/:userId", func(c *gin.Context) {

		var userId string = c.Param("userId")
		var postBody struct {
			FcmToken string `json:"fcmToken"`
		}

		err := c.Bind(&postBody)
		if err != nil {
			log.Println(err)
		}

		response := saveFcmToken(userId, postBody.FcmToken)

		c.JSON(200, response)
	})

	r.DELETE("/user/:userId", func(c *gin.Context) {

		var userId string = c.Param("userId")
		deleteResult := DeleteUser(userId)

		c.JSON(200, deleteResult)
	})

	r.Run()
}

func init() {
	initMundus()
}

func getJson() string {
	/*return `
	[
	  {
			  "firstname": "Yamani",
			  "lastname": "ADAME",
			  "email": "yamani_adame@hotmail.fr",
			  "password": "password",
			  "mobile": "060504030201",
			  "avatarImgUrl": "https://url.com",
			  "presantationImgUrl": "https://url.com",
			  "address": {
					"streetName": "Avenue du corbusier",
					"streetNumber": "23",
					"city": "Paris",
					"zipCode": "95800",
					"country": "France"
				  },
			  "creationDate": "09-06-2018",
			  "modificationDate": "09-06-2018"
			}
	]
	`*/

	return ``
}
