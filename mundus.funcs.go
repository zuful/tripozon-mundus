package main

import (
	"cloud.google.com/go/firestore"
	"context"
	"encoding/json"
	"firebase.google.com/go"
	auth2 "firebase.google.com/go/auth"
	"firebase.google.com/go/messaging"
	"github.com/satori/go.uuid"
	"google.golang.org/api/option"
	"log"
)

var userCollectionName string = "users"

var app *firebase.App
var firebaseErr error
var authFirebase *auth2.Client
var firestoreClient *firestore.Client
var ctx context.Context

func initMundus() {

	//Connection to Firebase
	ctx = context.Background()
	opt := option.WithCredentialsFile("./messenger-1670c.json")
	app, firebaseErr = firebase.NewApp(ctx, nil, opt)

	if firebaseErr != nil {
		log.Fatal(firebaseErr)
	}

	//Fireauth
	authFirebase, firebaseErr = app.Auth(ctx)
	if firebaseErr != nil {
		log.Fatal(firebaseErr)
	}

	//Firestore
	firestoreClient, firebaseErr = app.Firestore(ctx)
	if firebaseErr != nil {
		log.Fatal(firebaseErr)
	}
}

func CreateUser(user *UserStructure) error {

	_, err := firestoreClient.Collection(userCollectionName).Doc(user.Id).Set(ctx, user)
	if err != nil {
		log.Println(err)
	}

	return err
}

func GetUser(userId string) *UserStructure {

	var user *UserStructure

	userSnap, err := firestoreClient.Collection(userCollectionName).Doc(userId).Get(ctx)
	if err != nil {
		log.Println(err)
	}

	userMap := userSnap.Data()
	userJson, err := json.Marshal(userMap)
	if err != nil {
		log.Println(err)
	}

	err = json.Unmarshal([]byte(userJson), &user)
	if err != nil {
		log.Println(err)
	}

	setUserVerificationValues(user)

	return user
}

func setUserVerificationValues(user *UserStructure) {

	//assign disable information from fire auth
	userFromFireAuth, err := getUserFromFirebaseAuth(user.Id)
	if err != nil {
		log.Println(err)
	}

	//for some reason if email verified or is disabled equal false the field won't appear on the result
	if userFromFireAuth != nil {
		user.EmailVerified = userFromFireAuth.EmailVerified
		user.IsDisabled = userFromFireAuth.Disabled
	} else {
		user.IsDisabled = true
		user.EmailVerified = false
	}

}

func getUserFromFirebaseAuth(userId string) (*auth2.UserRecord, error) {
	user, err := authFirebase.GetUser(ctx, userId)
	if err != nil {
		log.Println(err)
	}

	return user, err
}

func UpdateUser(userId string, user UserStructure) *firestore.WriteResult {
	var updates []firestore.Update = []firestore.Update{
		{Path: "Firstname", Value: user.Firstname},
		{Path: "Lastname", Value: user.Lastname},
		{Path: "Mobile", Value: user.Mobile},
		{Path: "ModificationDate", Value: user.ModificationDate},
	}

	res, err := firestoreClient.Collection(userCollectionName).Doc(userId).Update(ctx, updates)
	if err != nil {
		log.Println(err)
	}

	return res
}

func DeleteUser(userId string) *firestore.WriteResult {

	res, err := firestoreClient.Collection(userCollectionName).Doc(userId).Delete(ctx)
	if err != nil {
		log.Fatal(err)
	}

	return res
}

func setUserId(user *UserStructure) {
	if user.Id == "" {
		user.Id = uuid.NewV4().String()
	}
}

func SignUp(user *UserStructure) error {

	var userToCreate *auth2.UserToCreate = new(auth2.UserToCreate)

	userToCreate.UID(user.Id)
	userToCreate.Email(user.Email)
	userToCreate.Password(user.Password)

	_, err := authFirebase.CreateUser(context.Background(), userToCreate)

	return err
}

func saveFcmToken(userId string, fcmToken string) *firestore.WriteResult {
	var updates []firestore.Update = []firestore.Update{
		{Path: "FcmToken", Value: fcmToken},
	}

	res, err := firestoreClient.Collection(userCollectionName).Doc(userId).Update(ctx, updates)
	if err != nil {
		log.Println(err)
	}

	return res
}

func sendFcmNotification(notification NotificationStructure) string {

	var message messaging.Message = messaging.Message{
		Token: notification.UserToken,
		Notification: &messaging.Notification{
			Title: notification.Title,
			Body:  notification.Body,
		},
		Webpush: &messaging.WebpushConfig{
			Notification: &messaging.WebpushNotification{
				Icon: notification.Icon,
			},
		},
	}

	fcmMessaging, err := app.Messaging(ctx)
	if err != nil {
		log.Println(err)
	}

	send, err := fcmMessaging.Send(ctx, &message)
	if err != nil {
		log.Println(err)
	}

	return send
}
