package main

type NotificationStructure struct {
	UserToken string `protobuf:"bytes,1,opt,name=userToken,proto3" json:"userToken,omitempty"`
	Title     string `protobuf:"bytes,1,opt,name=title,proto3" json:"title,omitempty"`
	Body      string `protobuf:"bytes,3,opt,name=body,proto3" json:"body,omitempty"`
	Icon      string `protobuf:"bytes,4,opt,name=icon,proto3" json:"icon,omitempty"`
}
